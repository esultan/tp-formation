package sopra.formation.dao.xml;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import sopra.formation.dao.IEleveDao;
import sopra.formation.model.Eleve;

public class EleveDaoXml implements IEleveDao {

	private final String chemin;

	public EleveDaoXml(String chemin) {
		super();
		this.chemin = chemin;
	}

	@Override
	public List<Eleve> findAll() {
		return load();
	}

	@Override
	public Eleve findById(Long id) {
		for (Eleve eleve : load()) {
			if (eleve.getId() == id) {
				return eleve;
			}
		}

		return null;
	}

	@Override
	public void create(Eleve obj) {
		List<Eleve> eleves = load();

		eleves.add(obj);

		save(eleves);
	}

	@Override
	public void update(Eleve obj) {
		List<Eleve> eleves = load();

		int i = 0;

		for (; i < eleves.size(); i++) {
			Eleve eleve = eleves.get(i);

			if (eleve.getId() == obj.getId()) {
				break;
			}
		}

		eleves.set(i, obj);

		save(eleves);
	}

	@Override
	public void delete(Eleve obj) {
		List<Eleve> eleves = load();

		eleves.remove(obj);

		save(eleves);
	}

	@Override
	public void deleteById(Long id) {
		List<Eleve> eleves = load();

		int i = 0;

		for (; i < eleves.size(); i++) {
			Eleve eleve = eleves.get(i);

			if (eleve.getId() == id) {
				break;
			}
		}

		eleves.remove(i);

		save(eleves);
	}

	@SuppressWarnings("unchecked")
	private List<Eleve> load() {
		List<Eleve> eleves = null;

		XMLDecoder decoder = null;
		try {
			decoder = new XMLDecoder(new FileInputStream(chemin));

			eleves = (List<Eleve>) decoder.readObject();
		} catch (FileNotFoundException e) {
			eleves = new ArrayList<Eleve>();
		} finally {
			if (decoder != null) {
				decoder.close();
			}
		}

		return eleves;
	}

	private void save(List<Eleve> eleves) {
		XMLEncoder encoder = null;
		try {
			encoder = new XMLEncoder(new FileOutputStream(chemin));
			encoder.writeObject(eleves);
			encoder.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if(encoder != null) {
				encoder.close();
			}
		}
	}

}
