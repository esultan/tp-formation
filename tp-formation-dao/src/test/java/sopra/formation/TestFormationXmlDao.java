package sopra.formation;

import sopra.formation.dao.IEleveDao;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;

public class TestFormationXmlDao {
	public static void main(String[] args) {
		IEleveDao eleveDao = Application.getInstance().getEleveDao();

		Eleve mehdi = new Eleve();
		mehdi.setId(3L);
		mehdi.setCivilite(Civilite.MLLE);
		mehdi.setNom("LANG");
		mehdi.setPrenom("Mehdi");
		mehdi.setAge(25);

		eleveDao.create(mehdi);

	}
}
