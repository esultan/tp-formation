package sopra.formation.dao;

import sopra.formation.model.Formateur;

public interface IFormateurRepository extends IRepository<Formateur, Long>{

}
