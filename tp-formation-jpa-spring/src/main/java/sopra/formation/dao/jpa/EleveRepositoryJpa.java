package sopra.formation.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.model.Eleve;

@Repository
@Transactional
public class EleveRepositoryJpa implements IEleveRepository {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public List<Eleve> findAll() {
		TypedQuery<Eleve> query = em.createQuery("select e from Eleve e", Eleve.class);
		
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Eleve findById(Long id) {
		return em.find(Eleve.class, id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Eleve findByIdWithFormateur(Long id) {
		TypedQuery<Eleve> query = em.createQuery("select distinct e from Eleve e left join fetch e.formateur f where e.id = :id", Eleve.class);
		
		query.setParameter("id", id);
		
		return query.getSingleResult();
	}

	@Override
	@Transactional(readOnly = true)
	public Eleve findByNomAndPrenom(String nom, String prenom) {
		TypedQuery<Eleve> query = em.createQuery("select e from Eleve e where e.nom = :leNom and e.prenom = :lePrenom",
				Eleve.class);

		query.setParameter("leNom", nom);
		query.setParameter("lePrenom", prenom);

		return query.getSingleResult();
	}

	@Override
	public Eleve save(Eleve obj) {
		return em.merge(obj);
	}

	@Override
	public void delete(Eleve obj) {
		em.remove(em.merge(obj));
	}

}
