package sopra.formation.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import sopra.formation.dao.IFormationRepository;
import sopra.formation.dao.IMatiereRepository;
import sopra.formation.model.Formation;
import sopra.formation.model.Matiere;
import sopra.formation.model.Views;

@RestController
@RequestMapping("/matiere")
@CrossOrigin("*")
public class MatiereController {

	@Autowired
	private IMatiereRepository matiereRepo;
	
	@Autowired
	private IFormationRepository formationRepo;

	@GetMapping("")
	@JsonView(Views.ViewMatiere.class)
	public List<Matiere> list() {
		return matiereRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewMatiere.class)
	public Matiere find(@PathVariable Long id) {
		return matiereRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewMatiere.class)
	public void create(@RequestBody Matiere matiere) {
		matiereRepo.save(matiere);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewMatiere.class)
	public void update(@PathVariable Long id, @RequestBody Matiere matiere) {
		matiereRepo.save(matiere);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		matiereRepo.deleteById(id);
	}
	
	@GetMapping("/{id}/formations")
	@JsonView(Views.ViewMatiere.class)
	public List<Formation> listFormation(@PathVariable Long id) {
		return formationRepo.findAllByMatiere(id);
	}

}
