package sopra.formation.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "training")
@IdClass(FormationId.class)
@NamedQueries({
		@NamedQuery(name = "Formation.rechercheParDateDeDebut", query = "select f from Formation f where f.dtDebut = :dateDebut"),
		@NamedQuery(name = "Formation.rechercheParDateDeFin", query = "select f from Formation f where f.dtFin = :dtFin")})
public class Formation {
	@Id
	@Column(length = 100)
	private String promotion;
	@Id
	@Column(name = "title", length = 100)
	private String intitule;
	@Column(name = "start_date")
	@Temporal(TemporalType.DATE)
	private Date dtDebut;
	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	private Date dtFin;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "training_subject", joinColumns = {
			@JoinColumn(name = "training_promotion", referencedColumnName = "promotion"),
			@JoinColumn(name = "training_title", referencedColumnName = "title") }, inverseJoinColumns = @JoinColumn(name = "subject_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
					"training_promotion", "training_title", "subject_id" }))
	private List<Matiere> matieres = new ArrayList<Matiere>();
	@OneToMany(mappedBy = "formation", fetch = FetchType.LAZY)
	private List<Eleve> eleves = new ArrayList<Eleve>();

	public Formation() {

	}

	public Formation(String promotion) {
		this.promotion = promotion;
	}

	public Formation(String promotion, String intitule, Date dtDebut, Date dtFin) {
		this.promotion = promotion;
		this.intitule = intitule;
		this.dtDebut = dtDebut;
		this.dtFin = dtFin;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Date getDtDebut() {
		return dtDebut;
	}

	public void setDtDebut(Date dtDebut) {
		this.dtDebut = dtDebut;
	}

	public Date getDtFin() {
		return dtFin;
	}

	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}

	public List<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<Matiere> matieres) {
		this.matieres = matieres;
	}

	public List<Eleve> getEleves() {
		return eleves;
	}

	public void setEleves(List<Eleve> eleves) {
		this.eleves = eleves;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Formation other = (Formation) obj;
		if (dtDebut == null) {
			if (other.dtDebut != null)
				return false;
		} else if (!dtDebut.equals(other.dtDebut))
			return false;
		if (dtFin == null) {
			if (other.dtFin != null)
				return false;
		} else if (!dtFin.equals(other.dtFin))
			return false;
		if (intitule == null) {
			if (other.intitule != null)
				return false;
		} else if (!intitule.equals(other.intitule))
			return false;
		if (promotion == null) {
			if (other.promotion != null)
				return false;
		} else if (!promotion.equals(other.promotion))
			return false;
		return true;
	}

}
