package sopra.formation.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import sopra.formation.dao.ISalleRepository;
import sopra.formation.model.Salle;
import sopra.formation.model.SalleId;
import sopra.formation.model.Views;

@RestController
@RequestMapping("/salle")
@CrossOrigin("*")
public class SalleController {

	@Autowired
	private ISalleRepository salleRepo;

	@GetMapping("")
	@JsonView(Views.ViewSalle.class)
	public List<Salle> list() {
		return salleRepo.findAll();
	}

	@GetMapping("/{nom}|{etage}") // clé composé (SalleId.class)
	@JsonView(Views.ViewSalle.class)
	public Salle find(@PathVariable String nom, @PathVariable Integer etage) {
		return salleRepo.findById(new SalleId(nom, etage)).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewSalle.class)
	public void create(@RequestBody Salle salle) {
		salleRepo.save(salle);
	}

	@PutMapping("/{nom}|{etage}")
	@JsonView(Views.ViewSalle.class)
	public void update(@PathVariable String nom, @PathVariable Integer etage, @RequestBody Salle salle) {
		salleRepo.save(salle);
	}

	@DeleteMapping("/{nom}|{etage}")
	@JsonView(Views.ViewSalle.class)
	public void remove(@PathVariable String nom, @PathVariable Integer etage) {
		salleRepo.deleteById(new SalleId(nom, etage));
	}

}
