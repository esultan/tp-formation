package sopra.formation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sopra.formation.model.Eleve;

public interface IEleveRepository extends JpaRepository<Eleve, Long> {
	Eleve findByNomAndPrenom(String nom, String prenom);

	@Query("select e from Eleve e left join fetch e.formateur f where e.id = :monId")
	Eleve findByIdWithFormateur(@Param("monId") Long id);
	
	@Query("select e from Eleve e where e.nom like :nom and e.prenom like :prenom")
	List<Eleve> findAllByNomAndPrenom(@Param("nom") String nom, @Param("prenom") String prenom);
}
