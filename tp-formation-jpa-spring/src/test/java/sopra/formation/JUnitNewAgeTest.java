package sopra.formation;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formateur;

public class JUnitNewAgeTest {
	private static ClassPathXmlApplicationContext context = null;

	@Test
	public void formateur() {
//		Assert.assertEquals("toto", "toto");
	}

	@Test
	public void eleve() {
		IEleveRepository eleveRepo = context.getBean(IEleveRepository.class);
		IFormateurRepository formateurRepo =  context.getBean(IFormateurRepository.class);
		
		Formateur formateur = new Formateur("SULTAN");
		
		formateur = formateurRepo.save(formateur);

		int startSize = eleveRepo.findAll().size();

		Eleve eleve = new Eleve();
		eleve.setCivilite(Civilite.MLLE);
		eleve.setNom("LANG");
		eleve.setPrenom("Mehdi");
		eleve.setAge(25);
		eleve.setFormateur(formateur);

		eleve = eleveRepo.save(eleve);

		eleve = eleveRepo.findById(eleve.getId());

		Assert.assertEquals(Civilite.MLLE, eleve.getCivilite());
		Assert.assertEquals("LANG", eleve.getNom());
		Assert.assertEquals("Mehdi", eleve.getPrenom());
		Assert.assertEquals(25, eleve.getAge());

		eleve.setCivilite(Civilite.MME);
		eleve.setNom("AUBRY");
		eleve.setPrenom("Laure");
		eleve.setAge(98);

		eleve = eleveRepo.save(eleve);

		eleve = eleveRepo.findById(eleve.getId());

		Assert.assertEquals(Civilite.MME, eleve.getCivilite());
		Assert.assertEquals("AUBRY", eleve.getNom());
		Assert.assertEquals("Laure", eleve.getPrenom());
		Assert.assertEquals(98, eleve.getAge());

		eleve = eleveRepo.findByNomAndPrenom("AUBRY", "Laure");

		Assert.assertNotNull(eleve);
		
		eleve = eleveRepo.findByIdWithFormateur(eleve.getId());
		
		Assert.assertEquals("SULTAN", eleve.getFormateur().getNom());

		int middleSize = eleveRepo.findAll().size();

		Assert.assertEquals(1, middleSize - startSize);

		eleveRepo.delete(eleve);

		int endSize = eleveRepo.findAll().size();

		Assert.assertEquals(0, endSize - startSize);
	}
	
	@BeforeClass
	public static void start() {
		context = new ClassPathXmlApplicationContext("application-context.xml");
	}
	
	@AfterClass
	public static void end() {
		context.close();
	}
}
