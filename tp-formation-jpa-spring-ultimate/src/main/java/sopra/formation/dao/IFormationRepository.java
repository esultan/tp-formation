package sopra.formation.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import sopra.formation.model.Formation;
import sopra.formation.model.FormationId;

public interface IFormationRepository extends JpaRepository<Formation, FormationId> {
	List<Formation> rechercheParDateDeDebut(@Param("dateDebut") Date debut);
	List<Formation> rechercheParDateDeFin(@Param("dtFin") Date fin);
}
