package sopra.formation.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Formateur;
import sopra.formation.model.Views;

@RestController
@RequestMapping("/formateur")
@CrossOrigin("*")
public class FormateurController {

	@Autowired
	private IFormateurRepository formateurRepo;

	@GetMapping("")
	@JsonView(Views.ViewFormateur.class)
	public List<Formateur> list() {
		return formateurRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewFormateur.class)
	public Formateur find(@PathVariable Long id) {
		return formateurRepo.findById(id).get();
	}

	@PostMapping("")
	@JsonView(Views.ViewFormateur.class)
	public void create(@RequestBody Formateur formateur) {
		formateurRepo.save(formateur);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewFormateur.class)
	public void update(@PathVariable Long id, @RequestBody Formateur formateur) {
		formateurRepo.save(formateur);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		formateurRepo.deleteById(id);
	}
	
	

}
