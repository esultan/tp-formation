package sopra.formation.dao.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import sopra.formation.Application;
import sopra.formation.dao.IEleveDao;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formateur;
import sopra.formation.model.Formation;

public class EleveDaoCsv implements IEleveDao {

	private final String chemin;

	public EleveDaoCsv(String chemin) {
		super();
		this.chemin = chemin;
	}

	@Override
	public List<Eleve> findAll() {
		return load();
	}

	@Override
	public Eleve findById(Long id) {
		for (Eleve eleve : load()) {
			if (eleve.getId() == id) {
				return eleve;
			}
		}

		return null;
	}

	@Override
	public void create(Eleve obj) {
		List<Eleve> eleves = load();

		eleves.add(obj);

		save(eleves);
	}

	@Override
	public void update(Eleve obj) {
		List<Eleve> eleves = load();

		int i = 0;

		for (; i < eleves.size(); i++) {
			Eleve eleve = eleves.get(i);

			if (eleve.getId() == obj.getId()) {
				break;
			}
		}

		eleves.set(i, obj);

		save(eleves);
	}

	@Override
	public void delete(Eleve obj) {
		List<Eleve> eleves = load();

		eleves.remove(obj);

		save(eleves);
	}

	@Override
	public void deleteById(Long id) {
		List<Eleve> eleves = load();

		int i = 0;

		for (; i < eleves.size(); i++) {
			Eleve eleve = eleves.get(i);

			if (eleve.getId() == id) {
				break;
			}
		}

		eleves.remove(i);

		save(eleves);
	}

	private List<Eleve> load() {
		List<Eleve> eleves = new ArrayList<Eleve>();

		Path path = Paths.get(chemin);

		List<String> lines = new ArrayList<String>();

		try {
			lines = Files.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String line : lines) {
			String[] items = line.split(";", -1);

			Eleve eleve = new Eleve();
			eleve.setId(Long.valueOf(items[0]));
			eleve.setCivilite(Civilite.valueOf(items[1]));
			eleve.setNom(items[2]);
			eleve.setPrenom(items[3]);
			eleve.setAge(Integer.valueOf(items[4]));

			if (items[5] != null && items[5].length() > 0) {
				String promotion = items[5];
				Formation formation = Application.getInstance().getFormationDao().findById(promotion);
				eleve.setFormation(formation);
			}

			if (items[6] != null && items[6].length() > 0) {
				Long formateurId = Long.valueOf(items[6]);
				Formateur formateur = Application.getInstance().getFormateurDao().findById(formateurId);
				eleve.setFormateur(formateur);
			}

			eleves.add(eleve);
		}

		return eleves;
	}

	private void save(List<Eleve> eleves) {
		List<String> lines = new ArrayList<String>();

		for (Eleve eleve : eleves) {
			StringJoiner joiner = new StringJoiner(";");
			joiner.add(eleve.getId().toString()).add(eleve.getCivilite().toString()).add(eleve.getNom())
					.add(eleve.getPrenom()).add(String.valueOf(eleve.getAge()));

			if (eleve.getFormation() != null) {
				joiner.add(eleve.getFormation().getPromotion());
			} else {
				joiner.add("");
			}

			if (eleve.getFormateur() != null) {
				joiner.add(eleve.getFormateur().getId().toString());
			} else {
				joiner.add("");
			}

			lines.add(joiner.toString());
		}

		Path path = Paths.get(chemin);

		try {
			Files.write(path, lines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
