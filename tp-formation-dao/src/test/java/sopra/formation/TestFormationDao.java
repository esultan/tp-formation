package sopra.formation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import sopra.formation.dao.IEleveDao;
import sopra.formation.dao.IFormationDao;
import sopra.formation.dao.csv.EleveDaoCsv;
import sopra.formation.dao.csv.FormationDaoCsv;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formation;

public class TestFormationDao {
	public static void main(String[] args) {
		IEleveDao eleveDao = Application.getInstance().getEleveDao();

		List<Eleve> eleves = eleveDao.findAll();

		for (Eleve eleve : eleves) {
			System.out.println(eleve.getNom());
		}

		System.out.println("##############################");

		Eleve laure = eleveDao.findById(2L);

		System.out.println(laure.getPrenom());

		System.out.println("##############################");

		Eleve mehdi = new Eleve();
		mehdi.setId(3L);
		mehdi.setCivilite(Civilite.MLLE);
		mehdi.setNom("LANG");
		mehdi.setPrenom("Mehdi");
		mehdi.setAge(25);

		eleveDao.create(mehdi);

		eleves = eleveDao.findAll();

		for (Eleve eleve : eleves) {
			System.out.println(eleve.getNom());
		}

		System.out.println("##############################");

		laure.setNom("SABAROT");
		laure.setCivilite(Civilite.MME);

		eleveDao.update(laure);

		laure = eleveDao.findById(2L);

		System.out.println(laure.getCivilite().getLabel() + " " + laure.getNom());

		System.out.println("##############################");

		eleveDao.delete(mehdi);

		eleves = eleveDao.findAll();

		for (Eleve eleve : eleves) {
			System.out.println(eleve.getNom());
		}

//		IFormationDao formationDao = Application.getInstance().getFormationDao();
//		
//		for(Formation formation : formationDao.findAll()) {
//			System.out.println(formation.getPromotion());
//		}
//		
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		
//		Formation cookie = new Formation("cookie");
//		
//		cookie.setIntitule("java spring emberjs");
//		try {
//			cookie.setDtDebut(sdf.parse("20190923"));
//			cookie.setDtFin(sdf.parse("20191214"));
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		formationDao.create(cookie);
	}
}
