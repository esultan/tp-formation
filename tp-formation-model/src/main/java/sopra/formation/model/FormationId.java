package sopra.formation.model;

import java.io.Serializable;

public class FormationId implements Serializable {
	private String promotion;
	private String intitule;

	public FormationId() {
		super();
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

}
