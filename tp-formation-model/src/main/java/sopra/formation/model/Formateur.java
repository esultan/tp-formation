package sopra.formation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "trainer")
public class Formateur {
	@Id
	@GeneratedValue
	private Long id;
	@Version
	private int version;
	@Column(name = "last_name", length = 100)
	@NotEmpty(message = "{formateur.form.nom.required}")
	@Size(min = 1, max = 100, message = "{formateur.form.nom.size}")
	@Pattern(regexp = "^[A-Z\\-]+$", message = "{formateur.form.nom.required}")
	private String nom;
	@Column(name = "first_name", length = 100)
	@NotEmpty(message = "{formateur.form.prenom.required}")
	@Size(min = 1, max = 100, message = "{formateur.form.prenom.size}")
	@Pattern(regexp = "^[a-z\\-]+$", message = "{formateur.form.prenom.pattern}")
	private String prenom;

	public Formateur() {
		super();
	}

	public Formateur(String nom) {
		this(nom, null);

	}

	public Formateur(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
