package sopra.formation.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sopra.formation.model.Formation;
import sopra.formation.model.FormationId;

public interface IFormationRepository extends JpaRepository<Formation, FormationId> {
	List<Formation> rechercheParDateDeDebut(@Param("dateDebut") Date debut);

	List<Formation> rechercheParDateDeFin(@Param("dtFin") Date fin);

	@Query("select distinct f from Formation f left join fetch f.eleves where f.promotion = :promotion and f.intitule = :intitule")
	Formation findByIdWithEleves(@Param("promotion") String promotion, @Param("intitule") String intitule);

	@Query("select distinct f from Formation f left join fetch f.matieres where f.promotion = :promotion and f.intitule = :intitule")
	Formation findByIdWithMatieres(@Param("promotion") String promotion, @Param("intitule") String intitule);
	
	@Query("select f from Formation f join f.matieres m where m.id = :id")
	List<Formation> findAllByMatiere(@Param("id") Long idMatiere);
}
