package sopra.formation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class TestSql {

	public static void main(String[] args) {
		int empId = 102;

		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "hr", "hr");

			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(
					"SELECT employee_id as empid, first_name, last_name, hire_date FROM employees WHERE employee_id = "
							+ empId);

			while (rs.next()) {
				int employeeId = rs.getInt("empid");
				String firstName = rs.getString("first_name");
				String lastName = rs.getString("last_name");
				Date hireDate = rs.getDate("hire_date");

				System.out.println(employeeId + "-" + firstName + "-" + lastName + "-" + hireDate);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		try {
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "hr", "hr");

			PreparedStatement ps = connection.prepareStatement(
					"SELECT employee_id as empid, first_name, last_name, hire_date FROM employees WHERE employee_id = ?");

			ps.setInt(1, empId);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				int employeeId = rs.getInt("empid");
				String firstName = rs.getString("first_name");
				String lastName = rs.getString("last_name");
				Date hireDate = rs.getDate("hire_date");

				System.out.println(employeeId + "-" + firstName + "-" + lastName + "-" + hireDate);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
