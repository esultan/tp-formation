package sopra.formation.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import sopra.formation.Application;
import sopra.formation.dao.IEleveDao;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formation;

public class EleveDaoSql implements IEleveDao {

	@Override
	public List<Eleve> findAll() {
		List<Eleve> eleves = new ArrayList<Eleve>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT id, civilite, nom, prenom, age, formation_promotion FROM eleve");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id = rs.getLong("id");
				Civilite civilite = Civilite.valueOf(rs.getString("civilite"));
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				int age = rs.getInt("age");
				String formationPromotion = rs.getString("formation_promotion");

				Eleve eleve = new Eleve();
				eleve.setId(id);
				eleve.setCivilite(civilite);
				eleve.setNom(nom);
				eleve.setPrenom(prenom);
				eleve.setAge(age);

				if (formationPromotion != null) {
					Formation formation = Application.getInstance().getFormationDao().findById(formationPromotion);
					eleve.setFormation(formation);
				}

				eleves.add(eleve);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return eleves;
	}

	@Override
	public Eleve findById(Long id) {
		Eleve eleve = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT  id, civilite, nom, prenom, age, formation_promotion FROM eleve WHERE id = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Civilite civilite = Civilite.valueOf(rs.getString("civilite"));
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				int age = rs.getInt("age");
				String formationPromotion = rs.getString("formation_promotion");

				eleve = new Eleve();
				eleve.setId(id);
				eleve.setCivilite(civilite);
				eleve.setNom(nom);
				eleve.setPrenom(prenom);
				eleve.setAge(age);

				if (formationPromotion != null) {
					Formation formation = Application.getInstance().getFormationDao().findById(formationPromotion);
					eleve.setFormation(formation);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return eleve;
	}

	@Override
	public void create(Eleve obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO eleve (id, civilite, nom, prenom, age, formation_promotion) VALUES (?,?,?,?,?,?)");

			ps.setLong(1, obj.getId());
			ps.setString(2, obj.getCivilite().name());
			ps.setString(3, obj.getNom());
			ps.setString(4, obj.getPrenom());
			ps.setInt(5, obj.getAge());
			
			if(obj.getFormation() != null && obj.getFormation().getPromotion() != null) {
				ps.setString(6, obj.getFormation().getPromotion());
			} else {
				ps.setNull(6, Types.VARCHAR);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Eleve obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("UPDATE eleve SET civilite = ?, nom = ?, prenom = ?, age = ?, promotion = ? WHERE id = ?");

			ps.setString(1, obj.getCivilite().name());
			ps.setString(2, obj.getNom());
			ps.setString(3, obj.getPrenom());
			ps.setInt(4, obj.getAge());
			
			if(obj.getFormation() != null && obj.getFormation().getPromotion() != null) {
				ps.setString(5, obj.getFormation().getPromotion());
			} else {
				ps.setNull(5, Types.VARCHAR);
			}
			
			ps.setLong(6, obj.getId());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Eleve obj) {
		deleteById(obj.getId());
	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE eleve WHERE id = ?");

			ps.setLong(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
