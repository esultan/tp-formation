package sopra.formation.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class SalleId implements Serializable {
	private String nom;
	private Integer etage;

	public SalleId() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getEtage() {
		return etage;
	}

	public void setEtage(Integer etage) {
		this.etage = etage;
	}

}
