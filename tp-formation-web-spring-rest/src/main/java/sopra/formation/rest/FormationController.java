package sopra.formation.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import sopra.formation.dao.IFormationRepository;
import sopra.formation.model.Formation;
import sopra.formation.model.FormationId;
import sopra.formation.model.Views;

@RestController
@RequestMapping("/formation")
@CrossOrigin("*")
public class FormationController {

	@Autowired
	private IFormationRepository formationRepo;

	@GetMapping("")
	@JsonView(Views.ViewFormation.class)
	public List<Formation> list() {
		return formationRepo.findAll();
	}

	@GetMapping("/{promotion}|{intitule}") // clé composé (FormationId.class)
	@JsonView(Views.ViewFormationWithEleves.class)
	public Formation find(@PathVariable String promotion, @PathVariable String intitule) {
		return formationRepo.findByIdWithEleves(promotion, intitule);
	}
	
	@GetMapping("/{promotion}|{intitule}/details") // clé composé (FormationId.class)
	@JsonView(Views.ViewFormationWithMatieres.class)
	public Formation detail(@PathVariable String promotion, @PathVariable String intitule) {
		return formationRepo.findByIdWithMatieres(promotion, intitule);
	}

	@PostMapping("")
	@JsonView(Views.ViewFormationWithMatieres.class)
	public void create(@RequestBody Formation formation) {
		formationRepo.save(formation);
	}

	@PutMapping("/{promotion}|{intitule}")
	@JsonView(Views.ViewFormationWithMatieres.class)
	public void update(@PathVariable String promotion, @PathVariable String intitule,
			@RequestBody Formation formation) {
		formationRepo.save(formation);
	}

	@DeleteMapping("/{promotion}|{intitule}")
	public void remove(@PathVariable String promotion, @PathVariable String intitule) {
		formationRepo.deleteById(new FormationId(promotion, intitule));
	}

}
