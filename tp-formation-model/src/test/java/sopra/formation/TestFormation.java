package sopra.formation;

import java.util.ArrayList;
import java.util.Date;

import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formateur;
import sopra.formation.model.Formation;
import sopra.formation.model.Matiere;

public class TestFormation {

	public static void main(String[] args) {
		Formation formation1 = new Formation();
		formation1.setPromotion("Antoinette");
		formation1.setIntitule("Sopra Java/Spring/Angular");
		formation1.setDtDebut(new Date(2019,5,19));
		formation1.setDtDebut(new Date(2019,8,20));
		
		Formation formation2 = new Formation("Antoinette");
		formation2.setIntitule("Sopra Java/Spring/Angular");
		formation2.setDtDebut(new Date(2019,5,19));
		formation2.setDtDebut(new Date(2019,8,20));
		System.out.println(formation1.equals(formation2));
		
		Matiere algo = new Matiere();
		algo.setNom("ALGORITHMIE");
		algo.setDuree(3);
		
		formation1.getMatieres().add(algo);
		
		String civiliteSimon = "M";
		
		Eleve simon = new Eleve("DUBOUREAU");
		simon.setCivilite(Civilite.valueOf(civiliteSimon));
		simon.setPrenom("Simon");
		simon.setAge(24);
		simon.setFormation(formation1);
		
		Eleve benjamin = new Eleve("COSTILLE");
		benjamin.setCivilite(Civilite.NSP);
		benjamin.setPrenom("Benjamin");
		benjamin.setAge(28);
		benjamin.setFormation(formation1);
		
		formation1.getEleves().add(simon);
		formation1.getEleves().add(benjamin);
		
		
		
		Formateur eric = new Formateur("SULTAN", "Eric");		
		
		simon.setFormateur(eric);
		benjamin.setFormateur(eric);
		
		Civilite[] civilites = Civilite.values();
		
		for(Civilite civ : civilites) {
			System.out.println(civ.getLabel());
		}
		
		
		

	}

}
