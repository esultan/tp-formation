package sopra.formation;

import sopra.formation.dao.IEleveDao;
import sopra.formation.dao.IFormateurDao;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formateur;

public class TestJpaWithDao {

	public static void main(String[] args) {
		IEleveDao eleveDao = Application.getInstance().getEleveDao();
		IFormateurDao formateurDao = Application.getInstance().getFormateurDao();
		
		Formateur eric = new Formateur(); 
		eric.setNom("SULTAN");
		eric.setPrenom("Eric");

		eric = formateurDao.save(eric);

		Eleve mehdi = new Eleve(); // new
		mehdi.setCivilite(Civilite.MLLE);
		mehdi.setNom("LANG");
		mehdi.setPrenom("Mehdi");
		mehdi.setAge(25);
		mehdi.setFormateur(eric);

		mehdi = eleveDao.save(mehdi); // (managed) -> detached

		System.out.println("id=" + mehdi.getId());
		
		mehdi.setNom("AUBRY"); // detached
		
		mehdi = eleveDao.save(mehdi); // (managed) -> detached
		
		mehdi = eleveDao.findById(mehdi.getId());
		
		System.out.println(mehdi.getFormateur().getNom());
		
//		eleveDao.delete(mehdi);

	}

}
