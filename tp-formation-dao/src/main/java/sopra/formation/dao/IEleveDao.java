package sopra.formation.dao;

import sopra.formation.model.Eleve;

public interface IEleveDao extends IDao<Eleve, Long> {
}
