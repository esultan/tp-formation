package sopra.formation;

import sopra.formation.model.Formateur;

public class TestFormateur {

	public static void main(String[] args) {
		Formateur eric = new Formateur();
		eric.setId(1L);
		eric.setNom("SULTAN");
		eric.setPrenom("Eric");
		
		Application.getInstance().getFormateurDao().create(eric);
		
		eric = Application.getInstance().getFormateurDao().findById(1L);
		
		System.out.println(eric.getNom()+" " +eric.getPrenom());
		
		eric.setNom("BRUEL");
		eric.setPrenom("Erica");
		
		Application.getInstance().getFormateurDao().update(eric);
		
		eric = Application.getInstance().getFormateurDao().findById(1L);
		
		System.out.println(eric.getNom()+" " +eric.getPrenom());
		
		Application.getInstance().getFormateurDao().delete(eric);
		
		eric = Application.getInstance().getFormateurDao().findById(1L);
		
		System.out.println(eric);

	}

}
