package sopra.formation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formateur;

public class TestJpaWithDao {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		
		IEleveRepository eleveRepo = context.getBean(IEleveRepository.class);
		IFormateurRepository formateurRepo =  context.getBean(IFormateurRepository.class);
		
		Formateur eric = new Formateur(); 
		eric.setNom("SULTAN");
		eric.setPrenom("Eric");

		eric = formateurRepo.save(eric);

		Eleve mehdi = new Eleve(); // new
		mehdi.setCivilite(Civilite.MLLE);
		mehdi.setNom("LANG");
		mehdi.setPrenom("Mehdi");
		mehdi.setAge(25);
		mehdi.setFormateur(eric);

		mehdi = eleveRepo.save(mehdi); // (managed) -> detached

		System.out.println("id=" + mehdi.getId());
		
		mehdi.setNom("AUBRY"); // detached
		
		mehdi = eleveRepo.save(mehdi); // (managed) -> detached
		
		mehdi = eleveRepo.findByIdWithFormateur(mehdi.getId());
		
		System.out.println(mehdi.getFormateur().getNom());
		
//		eleveDao.delete(mehdi);
		
		context.close();

	}

}
