package sopra.formation.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "student", uniqueConstraints = @UniqueConstraint(columnNames = { "last_name", "first_name" }))
public class Eleve {

	@Id
	@GeneratedValue
	@JsonView(Views.ViewCommon.class)
	private Long id;
	@Version
	@JsonView(Views.ViewCommon.class)
	private int version;
	@Column(name = "civility")
	@Enumerated(EnumType.STRING)
	@JsonView(Views.ViewCommon.class)
	private Civilite civilite;
	@Column(name = "last_name", length = 100)
	@JsonView(Views.ViewCommon.class)
	private String nom;
	@Column(name = "first_name", length = 100)
	@JsonView(Views.ViewCommon.class)
	private String prenom;
	@JsonView(Views.ViewCommon.class)
	private int age;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "trainer_id")
	@JsonView({ Views.ViewEleve.class, Views.ViewFormationWithEleves.class })
	private Formateur formateur;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "training_promotion", referencedColumnName = "promotion"),
			@JoinColumn(name = "training_title", referencedColumnName = "title") })
	@JsonView(Views.ViewEleveWithFormation.class)
	private Formation formation;
	@Column(length = 255)
	@JsonView(Views.ViewCommon.class)
	private String email;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonView(Views.ViewCommon.class)
	private Date dtEmbauche;

	public Eleve() {
		super();
	}

	public Eleve(String nom) {
		super();
		this.nom = nom;
	}

	public Eleve(Civilite civilite, String nom, String prenom, int age) {
		super();
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}

	public Eleve(Long id, int version, Civilite civilite, String nom, String prenom, int age) {
		super();
		this.id = id;
		this.version = version;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Formateur getFormateur() {
		return formateur;
	}

	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDtEmbauche() {
		return dtEmbauche;
	}

	public void setDtEmbauche(Date dtEmbauche) {
		this.dtEmbauche = dtEmbauche;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Eleve other = (Eleve) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
