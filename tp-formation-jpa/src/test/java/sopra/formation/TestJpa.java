package sopra.formation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import sopra.formation.model.Formateur;

public class TestJpa {

	public static void main(String[] args) {
		EntityManagerFactory emf = Application.getInstance().getEmf();

		Formateur eric = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
			em = emf.createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			eric = new Formateur(); // new ou transient
			eric.setNom("SULTAN");
			eric.setPrenom("Eric");

			em.persist(eric); // managed

			eric.setPrenom("Erica"); // managed

			tx.commit(); // em.flush();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		
		eric.setNom("RENOM"); // detached
		
		try {
			em = emf.createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			Formateur ericCopy = em.merge(eric); // ericCopy managed - eric detached

			Formateur ericLePremier = em.find(Formateur.class, 1L); // managed

			ericLePremier.setNom("ORTOLAN");

			em.remove(ericCopy); // removed

			em.persist(ericCopy); // managed

			tx.commit(); // em.flush();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}


		emf.close();
	}

}
