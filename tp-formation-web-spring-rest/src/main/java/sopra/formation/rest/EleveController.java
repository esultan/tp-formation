package sopra.formation.rest;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.model.Eleve;
import sopra.formation.model.Views;

@RestController
@RequestMapping("/eleve")
@CrossOrigin("*")
public class EleveController {

	@Autowired
	private IEleveRepository eleveRepo;

	@GetMapping("")
	@JsonView(Views.ViewEleve.class)
	public List<Eleve> list() {
		return eleveRepo.findAll();
	}

	@GetMapping("/{id}")
	@JsonView(Views.ViewEleveWithFormation.class)
	public Eleve find(@PathVariable Long id) {
		return eleveRepo.findByIdWithFormation(id);
	}

	@PostMapping("")
	@JsonView(Views.ViewEleve.class)
	public void create(@RequestBody Eleve eleve) {
		eleveRepo.save(eleve);
	}

	@PutMapping("/{id}")
	@JsonView(Views.ViewEleve.class)
	public void update(@PathVariable Long id, @RequestBody Eleve eleve) {
		eleveRepo.save(eleve);
	}

	@PatchMapping("/{id}")
	@JsonView(Views.ViewEleve.class)
	public void partialUpdate(@PathVariable Long id, @RequestBody Map<String, Object> fields) {
		Eleve eleve = eleveRepo.findById(id).get();

		for (String key : fields.keySet()) {
			Object value = fields.get(key);
			Field field = ReflectionUtils.findField(Eleve.class, key);
			ReflectionUtils.makeAccessible(field);
			ReflectionUtils.setField(field, eleve, value);
		}

		eleveRepo.save(eleve);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable Long id) {
		eleveRepo.deleteById(id);
	}
	
	

}
