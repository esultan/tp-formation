package sopra.formation.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;

@Controller
@RequestMapping("/eleve")
public class EleveController {
	@Autowired
	private IEleveRepository eleveRepo;
	@Autowired
	private IFormateurRepository formateurRepo;

	public EleveController() {
		super();

	}

	@RequestMapping(path = { "", "/list" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String list(@RequestParam(required = false) String nom, @RequestParam(required = false) String prenom,
			Model model) {

		if ((nom != null && !nom.isEmpty()) || (prenom != null && !prenom.isEmpty())) {
			model.addAttribute("mesEleves", eleveRepo.findAllByNomAndPrenom("%" + nom + "%", "%" + prenom + "%"));
			model.addAttribute("nom", nom);
			model.addAttribute("prenom", prenom);
		} else {
			model.addAttribute("mesEleves", eleveRepo.findAll());
		}

		return "eleve/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monEleve", new Eleve());
		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("formateurs", formateurRepo.findAll());

		return "eleve/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Eleve> optEleve = eleveRepo.findById(id);

		if (optEleve.isPresent()) {
			model.addAttribute("monEleve", optEleve.get());
		}

		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("formateurs", formateurRepo.findAll());

		return "eleve/form";
	}

//	@PostMapping("/save")
//	public String save(@RequestParam Long id, @RequestParam int version, @RequestParam Civilite civilite, @RequestParam String nom, @RequestParam String prenom, @RequestParam int age, @RequestParam String from) {
//
//		Eleve eleve = null;
//
//		if (from.contentEquals("add")) {
//			eleve = new Eleve(civilite, nom, prenom, age);
//		} else {
//			eleve = new Eleve(id, version, civilite, nom, prenom, age);
//		}
//
//		eleveRepo.save(eleve);
//
//		return "redirect:list";
//	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monEleve") @Valid Eleve eleve, BindingResult result, Model model) {
		new EleveValidator().validate(eleve, result);

		if (result.hasErrors()) {
			model.addAttribute("civilites", Civilite.values());
			model.addAttribute("formateurs", formateurRepo.findAll());

			return "eleve/form";
		}

		eleveRepo.save(eleve);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		eleveRepo.deleteById(id);

		return "redirect:/eleve/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
