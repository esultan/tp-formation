<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Elève</title>
<link rel="stylesheet" type="text/css"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="card">
			<div class="card-header bg-primary text-white">
				<h2>Liste des élèves</h2>
			</div>
			<div class="card-body table-wrap">
				<table id="eleveTab" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Civilité</th>
							<th>Nom</th>
							<th>Prénom</th>
							<th>Age</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
	
						<c:forEach items="${mesEleves}" var="eleve">
							<c:url value="/eleve/edit" var="editUrl">
								<c:param name="id" value="${eleve.id}"/>
							</c:url>
							<c:url value="/eleve/delete/${eleve.id}" var="deleteUrl"/>
							<tr>
								<td>${eleve.id}</td>
								<td>${eleve.civilite.label}</td>
								<td>${eleve.nom}</td>
								<td>${eleve.prenom}</td>
								<td>${eleve.age}</td>
								<td><a href="${editUrl}" class="btn btn-sm btn-primary"> <i
										class="fas fa-edit"></i>
								</a>&nbsp;<a href="${deleteUrl}" class="btn btn-sm btn-danger"> <i
										class="fas fa-trash"></i>
								</a></td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
			<c:url value="/eleve/add" var="addUrl"/>
			<div class="card-footer">
				<a href="${addUrl}" class="btn btn-primary"> <i
					class="fas fa-plus-square fa-2x"></i>
				</a>
			</div>
		</div>
	</div>
</body>
</html>