package sopra.formation;

import org.junit.Assert;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import junit.framework.TestCase;
import sopra.formation.dao.IEleveRepository;
import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;
import sopra.formation.model.Formateur;

public class JUnitOldSchoolTest extends TestCase {

	public void testFormateur() {
//		Assert.assertEquals("toto", "toto");
	}

	public void testEleve() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");

		IEleveRepository eleveRepo = context.getBean(IEleveRepository.class);
		IFormateurRepository formateurRepo =  context.getBean(IFormateurRepository.class);
		
		Formateur formateur = new Formateur("SULTAN");
		
		formateur = formateurRepo.save(formateur);

		int startSize = eleveRepo.findAll().size();

		Eleve eleve = new Eleve();
		eleve.setCivilite(Civilite.MLLE);
		eleve.setNom("LANG");
		eleve.setPrenom("Mehdi");
		eleve.setAge(25);
		eleve.setFormateur(formateur);

		eleve = eleveRepo.save(eleve);

		eleve = eleveRepo.findById(eleve.getId());

		Assert.assertEquals(Civilite.MLLE, eleve.getCivilite());
		Assert.assertEquals("LANG", eleve.getNom());
		Assert.assertEquals("Mehdi", eleve.getPrenom());
		Assert.assertEquals(25, eleve.getAge());

		eleve.setCivilite(Civilite.MME);
		eleve.setNom("AUBRY");
		eleve.setPrenom("Laure");
		eleve.setAge(98);

		eleve = eleveRepo.save(eleve);

		eleve = eleveRepo.findById(eleve.getId());

		Assert.assertEquals(Civilite.MME, eleve.getCivilite());
		Assert.assertEquals("AUBRY", eleve.getNom());
		Assert.assertEquals("Laure", eleve.getPrenom());
		Assert.assertEquals(98, eleve.getAge());

		eleve = eleveRepo.findByNomAndPrenom("AUBRY", "Laure");

		Assert.assertNotNull(eleve);
		
		eleve = eleveRepo.findByIdWithFormateur(eleve.getId());
		
		Assert.assertEquals("SULTAN", eleve.getFormateur().getNom());

		int middleSize = eleveRepo.findAll().size();

		Assert.assertEquals(1, middleSize - startSize);

		eleveRepo.delete(eleve);

		int endSize = eleveRepo.findAll().size();

		Assert.assertEquals(0, endSize - startSize);

		context.close();
	}
}
