package sopra.formation.dao.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import sopra.formation.dao.IFormateurDao;
import sopra.formation.model.Formateur;

public class FormateurDaoCsv implements IFormateurDao {

	private final String chemin;

	public FormateurDaoCsv(String chemin) {
		super();
		this.chemin = chemin;
	}

	@Override
	public List<Formateur> findAll() {
		return load();
	}

	@Override
	public Formateur findById(Long id) {
		for (Formateur formateur : load()) {
			if (formateur.getId() == id) {
				return formateur;
			}
		}

		return null;
	}

	@Override
	public void create(Formateur obj) {
		List<Formateur> formateurs = load();

		formateurs.add(obj);

		save(formateurs);
	}

	@Override
	public void update(Formateur obj) {
		List<Formateur> formateurs = load();

		int i = 0;

		for (; i < formateurs.size(); i++) {
			Formateur formateur = formateurs.get(i);

			if (formateur.getId() == obj.getId()) {
				break;
			}
		}

		formateurs.set(i, obj);

		save(formateurs);
	}

	@Override
	public void delete(Formateur obj) {
		List<Formateur> formateurs = load();

		formateurs.remove(obj);

		save(formateurs);
	}

	@Override
	public void deleteById(Long id) {
		List<Formateur> formateurs = load();

		int i = 0;

		for (; i < formateurs.size(); i++) {
			Formateur formateur = formateurs.get(i);

			if (formateur.getId() == id) {
				break;
			}
		}

		formateurs.remove(i);

		save(formateurs);
	}

	private List<Formateur> load() {
		List<Formateur> formateurs = new ArrayList<Formateur>();

		Path path = Paths.get(chemin);

		List<String> lines = new ArrayList<String>();

		try {
			lines = Files.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String line : lines) {
			String[] items = line.split(";");

			Formateur formateur = new Formateur();
			formateur.setId(Long.valueOf(items[0]));
			formateur.setNom(items[1]);
			formateur.setPrenom(items[2]);

			formateurs.add(formateur);
		}

		return formateurs;
	}

	private void save(List<Formateur> formateurs) {
		List<String> lines = new ArrayList<String>();

		for (Formateur formateur : formateurs) {
			StringJoiner joiner = new StringJoiner(";");
			joiner.add(formateur.getId().toString()).add(formateur.getNom()).add(formateur.getPrenom());

			lines.add(joiner.toString());
		}

		Path path = Paths.get(chemin);

		try {
			Files.write(path, lines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
