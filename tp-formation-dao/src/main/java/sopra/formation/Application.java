package sopra.formation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import sopra.formation.dao.IEleveDao;
import sopra.formation.dao.IFormateurDao;
import sopra.formation.dao.IFormationDao;
import sopra.formation.dao.csv.FormationDaoCsv;
import sopra.formation.dao.object.EleveDaoObject;
import sopra.formation.dao.sql.FormateurDaoSql;

public class Application {
	private static Application instance = null;

	private final IEleveDao eleveDao = new EleveDaoObject("eleves.ser");
	private final IFormateurDao formateurDao = new FormateurDaoSql();
	private final IFormationDao formationDao = new FormationDaoCsv("formations.csv");

	private Application() {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Application getInstance() {
		if (instance == null) {
			instance = new Application();
		}

		return instance;
	}

	public IEleveDao getEleveDao() {
		return eleveDao;
	}

	public IFormateurDao getFormateurDao() {
		return formateurDao;
	}

	public IFormationDao getFormationDao() {
		return formationDao;
	}

	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "formation", "formation");
	}

}
