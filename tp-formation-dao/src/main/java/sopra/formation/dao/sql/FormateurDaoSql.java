package sopra.formation.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sopra.formation.Application;
import sopra.formation.dao.IFormateurDao;
import sopra.formation.model.Formateur;

public class FormateurDaoSql implements IFormateurDao {

	@Override
	public List<Formateur> findAll() {
		List<Formateur> formateurs = new ArrayList<Formateur>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT id, nom, prenom FROM formateur");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id = rs.getLong("id");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");

				Formateur formateur = new Formateur();
				formateur.setId(id);
				formateur.setNom(nom);
				formateur.setPrenom(prenom);
				
				formateurs.add(formateur);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return formateurs;
	}

	@Override
	public Formateur findById(Long id) {
		Formateur formateur = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT id, nom, prenom FROM formateur WHERE id = ?");

			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");

				formateur = new Formateur();
				formateur.setId(id);
				formateur.setNom(nom);
				formateur.setPrenom(prenom);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return formateur;
	}

	@Override
	public void create(Formateur obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO formateur (id, nom, prenom) VALUES (?,?,?)");
			
			ps.setLong(1, obj.getId());
			ps.setString(2, obj.getNom());
			ps.setString(3, obj.getPrenom());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Formateur obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE formateur SET nom = ?, prenom = ? WHERE id = ?");
			
			ps.setString(1, obj.getNom());
			ps.setString(2, obj.getPrenom());
			ps.setLong(3, obj.getId());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Formateur obj) {
		deleteById(obj.getId());
	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE formateur WHERE id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
