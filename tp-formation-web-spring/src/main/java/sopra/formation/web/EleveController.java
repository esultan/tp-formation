package sopra.formation.web;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.model.Civilite;
import sopra.formation.model.Eleve;

@Controller
@RequestMapping("/eleve")
public class EleveController {
	@Autowired
	private IEleveRepository eleveRepo;

	public EleveController() {
		super();

	}

	@GetMapping({"","/list"})
	public String list(Model model)  {
		model.addAttribute("mesEleves", eleveRepo.findAll());
		
		return "eleve/list";
	}

	@GetMapping("/add")
	public String add(Model model)  {
		model.addAttribute("monEleve", new Eleve());
		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("from", "add");
		
		return "eleve/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Eleve> optEleve = eleveRepo.findById(id);

		if (optEleve.isPresent()) {
			model.addAttribute("monEleve", optEleve.get());
		}

		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("from", "edit");

		return "eleve/form";
	}

	@PostMapping("/save")
	public String save(@RequestParam Long id, @RequestParam int version, @RequestParam Civilite civilite, @RequestParam String nom, @RequestParam String prenom, @RequestParam int age, @RequestParam String from) {

		Eleve eleve = null;

		if (from.contentEquals("add")) {
			eleve = new Eleve(civilite, nom, prenom, age);
		} else {
			eleve = new Eleve(id, version, civilite, nom, prenom, age);
		}

		eleveRepo.save(eleve);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		eleveRepo.deleteById(id);

		return "redirect:list";
	}

	@GetMapping("/cancel")
	public String cancel() {
		return "forward:list";
	}

}
