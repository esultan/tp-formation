package sopra.formation.dao;

import sopra.formation.model.Eleve;

public interface IEleveRepository extends IRepository<Eleve, Long> {
	Eleve findByNomAndPrenom(String nom, String prenom);
	Eleve findByIdWithFormateur(Long id);
}
