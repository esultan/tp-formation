package sopra.formation.dao.object;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import sopra.formation.dao.IEleveDao;
import sopra.formation.model.Eleve;

public class EleveDaoObject implements IEleveDao {

	private final String chemin;

	public EleveDaoObject(String chemin) {
		super();
		this.chemin = chemin;
	}

	@Override
	public List<Eleve> findAll() {
		return load();
	}

	@Override
	public Eleve findById(Long id) {
		for (Eleve eleve : load()) {
			if (eleve.getId() == id) {
				return eleve;
			}
		}

		return null;
	}

	@Override
	public void create(Eleve obj) {
		List<Eleve> eleves = load();

		eleves.add(obj);

		save(eleves);
	}

	@Override
	public void update(Eleve obj) {
		List<Eleve> eleves = load();

		int i = 0;

		for (; i < eleves.size(); i++) {
			Eleve eleve = eleves.get(i);

			if (eleve.getId() == obj.getId()) {
				break;
			}
		}

		eleves.set(i, obj);

		save(eleves);
	}

	@Override
	public void delete(Eleve obj) {
		List<Eleve> eleves = load();

		eleves.remove(obj);

		save(eleves);
	}

	@Override
	public void deleteById(Long id) {
		List<Eleve> eleves = load();

		int i = 0;

		for (; i < eleves.size(); i++) {
			Eleve eleve = eleves.get(i);

			if (eleve.getId() == id) {
				break;
			}
		}

		eleves.remove(i);

		save(eleves);
	}

	@SuppressWarnings("unchecked")
	private List<Eleve> load() {
		List<Eleve> eleves = null;

		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			fis = new FileInputStream(chemin);
			ois = new ObjectInputStream(fis);

			eleves = (List<Eleve>) ois.readObject();
		} catch (FileNotFoundException e) {
			eleves = new ArrayList<Eleve>();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				ois.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
			}
		}

		return eleves;
	}

	private void save(List<Eleve> eleves) {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		try {
			fos = new FileOutputStream(chemin);
			oos = new ObjectOutputStream(fos);

			oos.writeObject(eleves);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				oos.close();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
			}

		}
	}

}
