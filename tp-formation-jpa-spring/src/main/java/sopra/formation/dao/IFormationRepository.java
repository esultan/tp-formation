package sopra.formation.dao;

import sopra.formation.model.Formation;
import sopra.formation.model.FormationId;

public interface IFormationRepository extends IRepository<Formation, FormationId> {

}
