package sopra.formation;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.model.Eleve;

public class TestSpring {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		
		IEleveRepository eleveRepo = context.getBean(IEleveRepository.class);
		
		List<Eleve> eleves = eleveRepo.findAll();
		
		context.close();

	}

}
