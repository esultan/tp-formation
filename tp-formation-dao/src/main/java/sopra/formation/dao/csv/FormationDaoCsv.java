package sopra.formation.dao.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

import sopra.formation.dao.IFormationDao;
import sopra.formation.model.Formation;

public class FormationDaoCsv implements IFormationDao {

	private final String chemin;
	private final SimpleDateFormat sdf;

	public FormationDaoCsv(String chemin) {
		super();
		this.chemin = chemin;
		this.sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	@Override
	public List<Formation> findAll() {
		return load();
	}

	@Override
	public Formation findById(String promo) {
		for (Formation formation : load()) {
			if (formation.getPromotion().equals(promo)) {
				return formation;
			}
		}

		return null;
	}

	@Override
	public void create(Formation obj) {
		List<Formation> formations = load();

		formations.add(obj);

		save(formations);
	}

	@Override
	public void update(Formation obj) {
		List<Formation> formations = load();

		int i = 0;

		for (; i < formations.size(); i++) {
			Formation promotion = formations.get(i);

			if (promotion.getPromotion().contentEquals(obj.getPromotion())) {
				break;
			}
		}

		formations.set(i, obj);

		save(formations);
	}

	@Override
	public void delete(Formation obj) {
		List<Formation> formations = load();

		formations.remove(obj);

		save(formations);
	}

	@Override
	public void deleteById(String id) {
		List<Formation> formations = load();

		int i = 0;

		for (; i < formations.size(); i++) {
			Formation formation = formations.get(i);

			if (formation.getPromotion().contentEquals(id)) {
				break;
			}
		}

		formations.remove(i);

		save(formations);
	}

	private List<Formation> load() {
		List<Formation> formations = new ArrayList<Formation>();

		Path path = Paths.get(chemin);

		List<String> lines = new ArrayList<String>();

		try {
			lines = Files.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String line : lines) {
			String[] items = line.split(";");

			Date dtDebut = null;
			Date dtFin = null;
			try {
				dtDebut = sdf.parse(items[2]);
				dtFin = sdf.parse(items[3]);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Formation formation = new Formation();
			formation.setPromotion(items[0]);
			formation.setIntitule(items[1]);
			formation.setDtDebut(dtDebut);
			formation.setDtFin(dtFin);

			formations.add(formation);
		}

		return formations;
	}

	private void save(List<Formation> formations) {
		List<String> lines = new ArrayList<String>();

		for (Formation formation : formations) {
			StringJoiner joiner = new StringJoiner(";");
			joiner.add(formation.getPromotion()).add(formation.getIntitule()).add(sdf.format(formation.getDtDebut()))
					.add(sdf.format(formation.getDtFin()));

			lines.add(joiner.toString());
		}

		Path path = Paths.get(chemin);

		try {
			Files.write(path, lines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
