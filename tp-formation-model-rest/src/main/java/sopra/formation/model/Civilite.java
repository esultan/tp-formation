package sopra.formation.model;

public enum Civilite {
	M("civilite.M"), MME("civilite.MME"), MLLE("civilite.MLLE"), NSP("civilite.NSP");

	private final String label;

	private Civilite(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
