<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Elève</title>
<link rel="stylesheet" type="text/css"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<div id="eleveEdit" class="card mt-5">
			<form action="<c:url value="/eleve/save"/>" method="post">
				<input type="hidden" name="from" value="${from}"/>
				<input type="hidden" name="version" value="${monEleve.version}"/>
				<div class="card-header">
					<h5>Edition de l'élève</h5>
				</div>
				<div class="card-body">

					<div class="form-group">
						<label for="id">Identifiant:</label> <input type="number"
							class="form-control" id="id" name="id" required value="${monEleve.id}" readonly>
					</div>
					<div class="form-group">
						<label for="civilite">Civilité:</label><select
							class="form-control" id="civilite" name="civilite">
							<c:forEach items="${civilites}" var="civ">
								<option value="${civ}" ${monEleve.civilite eq civ ? 'selected' : '' }>${civ.label}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group">
						<label for="nom">Nom:</label> <input type="text"
							class="form-control" id="nom" name="nom" value="${monEleve.nom}">
					</div>
					<div class="form-group">
						<label for="prenom">Prénom:</label> <input type="text"
							class="form-control" id="prenom" name="prenom" value="${monEleve.prenom}">
					</div>
					<div class="form-group">
						<label for="dtNaissance">Age:</label> <input type="number"
							class="form-control" id="age" name="age" value="${monEleve.age}" min="0" >
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="btn-group ">
						<button type="submit" class="btn btn-success">
							<i class="far fa-check-square fa-2x"></i>
						</button>
						<c:url value="/eleve/cancel" var="cancelUrl"/>
						<a href="${cancelUrl}" class="btn btn-warning text-white">
							<i class="far fa-window-close fa-2x"></i>
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>