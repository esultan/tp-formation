package sopra.formation.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sopra.formation.Application;
import sopra.formation.dao.IFormationDao;
import sopra.formation.model.Formation;

public class FormationDaoSql implements IFormationDao {

	@Override
	public List<Formation> findAll() {
		List<Formation> formations = new ArrayList<Formation>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("SELECT promotion, intitule, dt_debut, dt_fin FROM formation");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String promotion = rs.getString("promotion");
				String intitule = rs.getString("intitule");
				Date dtDebut = rs.getDate("dt_debut");
				Date dtFin = rs.getDate("dt_fin");

				Formation formation = new Formation();
				formation.setPromotion(promotion);
				formation.setIntitule(intitule);
				formation.setDtDebut(dtDebut);
				formation.setDtFin(dtFin);

				formations.add(formation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return formations;
	}

	@Override
	public Formation findById(String promotion) {
		Formation formation = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT promotion, intitule, dt_debut, dt_fin FROM formation WHERE promotion = ?");

			ps.setString(1, promotion);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String intitule = rs.getString("intitule");
				Date dtDebut = rs.getDate("dt_debut");
				Date dtFin = rs.getDate("dt_fin");

				formation = new Formation();
				formation.setPromotion(promotion);
				formation.setIntitule(intitule);
				formation.setDtDebut(dtDebut);
				formation.setDtFin(dtFin);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return formation;
	}

	@Override
	public void create(Formation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO formation (promotion, intitule, dt_debut, dt_fin) VALUES (?,?,?,?)");

			ps.setString(1, obj.getPromotion());
			ps.setString(2, obj.getIntitule());
			ps.setDate(3, new Date(obj.getDtDebut().getTime()));
			ps.setDate(4, new Date(obj.getDtFin().getTime()));

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Formation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE formation SET intitule = ?, dt_debut = ?, dt_fin = ? WHERE promotion = ?");

			ps.setString(1, obj.getIntitule());
			ps.setDate(2, new Date(obj.getDtDebut().getTime()));
			ps.setDate(3, new Date(obj.getDtFin().getTime()));
			ps.setString(4, obj.getPromotion());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Formation obj) {
		deleteById(obj.getPromotion());
	}

	@Override
	public void deleteById(String promotion) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE formation WHERE promotion = ?");

			ps.setString(1, promotion);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
