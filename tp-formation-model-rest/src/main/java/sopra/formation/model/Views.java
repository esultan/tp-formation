package sopra.formation.model;

public class Views {
	public static class ViewCommon {}
	
	public static class ViewEleve extends ViewCommon {}
	
	public static class ViewEleveWithFormation extends ViewEleve {}
	
	public static class ViewFormation extends ViewCommon {}
	
	public static class ViewFormationWithEleves extends ViewFormation {}
	
	public static class ViewFormationWithMatieres extends ViewFormation {}
	
	public static class ViewFormateur extends ViewCommon {}
	
	public static class ViewMatiere extends ViewCommon {}
	
	public static class ViewSalle extends ViewCommon {}
}
