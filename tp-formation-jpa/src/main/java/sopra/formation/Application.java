package sopra.formation;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sopra.formation.dao.IEleveDao;
import sopra.formation.dao.IFormateurDao;
import sopra.formation.dao.IFormationDao;
import sopra.formation.dao.jpa.EleveDaoJpa;
import sopra.formation.dao.jpa.FormateurDaoJpa;

public class Application {
	private static Application instance = null;

	private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("formation-tp");

	private final IEleveDao eleveDao = new EleveDaoJpa();
	private final IFormateurDao formateurDao = new FormateurDaoJpa();
	private final IFormationDao formationDao = null;

	private Application() {
	}

	public static Application getInstance() {
		if (instance == null) {
			instance = new Application();
		}

		return instance;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public IEleveDao getEleveDao() {
		return eleveDao;
	}

	public IFormateurDao getFormateurDao() {
		return formateurDao;
	}

	public IFormationDao getFormationDao() {
		return formationDao;
	}

}
