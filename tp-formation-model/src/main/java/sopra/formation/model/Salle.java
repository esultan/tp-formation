package sopra.formation.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "room")
public class Salle {
	@EmbeddedId
	private SalleId id;
	private Integer capacite;

	public Salle() {
		super();
	}

	public SalleId getId() {
		return id;
	}

	public void setId(SalleId id) {
		this.id = id;
	}

	public Integer getCapacite() {
		return capacite;
	}

	public void setCapacite(Integer capacite) {
		this.capacite = capacite;
	}

}
