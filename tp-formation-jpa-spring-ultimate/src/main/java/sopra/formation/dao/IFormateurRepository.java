package sopra.formation.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.formation.model.Formateur;

public interface IFormateurRepository extends JpaRepository<Formateur, Long>{
	List<Formateur> findByNom(String nom);
} 
