package sopra.formation;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import sopra.formation.config.AppConfig;
import sopra.formation.dao.IEleveRepository;
import sopra.formation.model.Eleve;

public class TestSpringByClass {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

IEleveRepository eleveRepo = context.getBean(IEleveRepository.class);
		
		List<Eleve> eleves = eleveRepo.findAll();
		
		context.close();

	}
}
