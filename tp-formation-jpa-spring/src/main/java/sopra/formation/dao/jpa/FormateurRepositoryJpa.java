package sopra.formation.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sopra.formation.dao.IEleveRepository;
import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Formateur;

@Repository
@Transactional
public class FormateurRepositoryJpa implements IFormateurRepository {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private IEleveRepository eleveDao;

	@Override
	@Transactional(readOnly = true)
	public List<Formateur> findAll() {
		TypedQuery<Formateur> query = em.createQuery("select f from Formateur f", Formateur.class);

		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Formateur findById(Long id) {
		return em.find(Formateur.class, id);
	}

	@Override
	public Formateur save(Formateur obj) {
		return em.merge(obj);
	}

	@Override
	public void delete(Formateur obj) {
		em.remove(em.merge(obj));
	}

}
